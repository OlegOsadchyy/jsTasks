/*

Написать функцию bindAll(obj) которая получает на вход объект вида
var complexNum = { r: 1, i: 2, mod: function() { return Math.sqrt(this.r * this.r + this.i * this.i); } };
И переделывает все его методы таким образом что код:
var mod= complexNum.mod;
mod() === complexNum.mod() вернет истину (то есть mod будет работать одинаково в обоих случаях)

*/



var complexNum = { r: 1, i: 2, mod: function() { 
  return Math.sqrt(this.r * this.r + this.i * this.i); 
} };

function bindAll (obj) {                                               // check the object
    for (var name in obj) {                                            // every property
        //console.log(name + ' has type ' + typeof(obj[name]));
        if (typeof(obj[name]) === 'function') {                         // and modify each method by modifyMethod function that wraps existed method with obj context
            var f = obj[name];
            obj[name] = function (){ return f.apply(obj) };
            return obj;
        };
    }
};

complexNum = bindAll(complexNum);

var mod = complexNum.mod;
console.log (complexNum.mod());             // 2.23606797749979
console.log (mod());                        // 2.23606797749979
console.log ( (mod()=== complexNum.mod())); // true