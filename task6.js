
/*
Предположим есть составной объект:
var obj = {
	x: 6,
	inner: {
		a: ‘test’,
		b: [1, 2, 3]
	}
}
Написать функцию extract(obj, strPath) которая извлекает данные из такого объекта по пути:
extract(obj, ‘x’) => 6
extract(obj, ‘inner.a’) => ‘test’
extract(obj, ‘inner.b[2]’) => 3
extract(obj, ‘inner[‘b’][2]’) => 3 (доступ к свойству объекта через [ ])
PS: eval использовать нельзя! (решение в этом случае - 1 строка)

*/

var obj = {
	x: 6,
	inner: {
		a: 'test',
		b: [1, 2, 3],
        test: {
            a: 'value',
            b: [1,[1,2],3]
        }
	}
}


function extract(obj, strPath){
    //console.log ('object = ' + obj );                 // need to prepare path first
    strPath = strPath.replace( /\[/g, '.');             // if path contains ' it should be removed 
    strPath = strPath.replace (/\]/g, '');              // [text] should be changed to .text
    strPath = strPath.replace (/\'/g, '');

    console.log ('modified path =' + strPath);
    var path = [];                                      // saving path parts seprated by . 
    path = strPath.split('.');                          // as elements of array and process path array recursively
    if (path[0]=='') {path.shift()};
    console.log (path);
    if (path.length == 1) {                             // if we have one element in path array then we need to stop processing 
       console.log (' the last one');
       console.log (path[0]);
        return (obj[path[0]]);                          // and return the value of the obj element by path value
    } else {
        var root = path.shift();                        // else we need to truncate path for 1 element and 
        console.log ('root = ' + root + ' ');// + path.join('.'));
        return (extract ( obj[root], path.join('.')) ); // call extract once again with new reuced path
    }
} 

var path = "inner.test['b'][1][0]";
var pathTwo = "inner['b'][0]";

//console.log (extract (obj, 'x'));       // 6
//console.log (extract (obj, path));      // 3
//console.log (extract (obj, pathTwo));   // 1
//console.log (extract (obj, "inner[test]['a']")); // value


var objNew = {
			a: 1,
			b: '2',
			c: {
				d: [1, 2, 3],
				e: null,
				f: 3
			}
		};

//console.log (extract(objNew, 'a'));
//console.log (extract(objNew, 'b'));
//console.log (extract(objNew, 'c'));
//console.log (extract(objNew, 'c.f')); //3
//console.log (extract(objNew, 'c.e')) //null);
//console.log (extract(objNew, 'c.d')) // obj.c.d;
//console.log (extract(objNew, 'c.d[1]')) // obj.c.d[1]);
//console.log (extract(objNew, "['a']"))//, obj.a);
console.log (extract(objNew, '[\'c\'][\'f\']')) // obj.c.f);
console.log (extract(objNew, '[\'c\'][\'d\'][2]')) // obj.c.d[2]);
	