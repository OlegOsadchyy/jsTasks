/*
Написать функцию carry которая на вход принимает функцию f с n аргументами (n >= 1) и возвращает функцию g с 1 аргументом. 
Причем, первоначальный вызов f с n аргументами можно выполнить путем n вызовов g c 1 аргументом. например:
function add3(a, b, c) { return a + b + c; }
var g = carry(add3);
g(1); g(2); g(3) => третий вызов вернет 6
добавить возможность “сцепления”
g(1)(2)(3); => 6

*/
function add3(a, b, c) { return a + b + c; }
function carry (fn) {
    //console.log ('fn=' + fn);
    var arg=[];
    function c(val) {
        arg.push(val);
        //console.log ('c. arg = ' + arg );
        //console.log ('fn.length = ' + fn.length);
        //console.log ('arg length = ' + arg.length);
        //console.log( fn.apply(this, arg) );
        //return fn.apply(this, arg);
            if(fn.length == arg.length){
             return fn.apply(this, arg);
            } else { return c; }
    }
    return c;
}
var g = carry(add3);
console.log (g(1)(2)(3));
var h = carry(add3);
h(1);
h(2);
console.log (h(3));