/*
Написать функцию seq которая на вход получает пользовательскую функцию f, 
а на выходе возвращает функцию, вызывая ее, каждый раз можно получить последовательность чисел, 
определяемую пользовательской функцией f. Функция f принимает на вход в качестве аргументов результаты всех своих предыдущих вызовов. 
Порядок аргументов такой: слева находится результат предыдущего вызова, потом результат пред-предыдущего и т.д. 
Например, для генерации чисел Фибоначчи можно использовать след код:
var fib = seq(function(n1, n2) { return (n1 + n2) || 1; });
fib() => 1
fib() => 1
fib() => 2
fib() => 3
fib() => 5 и т.д.
*/

// fib  
var fib = seq(function(n1, n2) { 
    //console.log ('f function here. n1 = ' + n1 + ' n2 = ' + n2);
    return (n1 + n2) || 1; });


function seq(func){
    var previousResults = [];                                       // an array of arguments for sequence function f. 
                                                                    // Items are results of previous f call. 

    var fib = function(){                                           // fib calls f with previous results and adds the array a new one
        previousResults.unshift(func.apply(this, previousResults)); // calling f function against the exited arguments strored in previousResults
                                                                    // adding new element with newly got result to the beginning of array
        return previousResults[0];                                  // return newly got result
    }
    return fib;                                                    
}

console.log (fib()); // 1
console.log (fib()); // 1
console.log (fib()); // 2
console.log (fib()); // 3
console.log (fib()); // 5
console.log (fib()); // 8
console.log (fib()); // 13
console.log (fib()); // 21