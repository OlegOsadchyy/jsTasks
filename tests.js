'use strict';

describe('bindAll', function() {
	let complexNum = null;
	let mod = null;
	beforeEach(function() {
		complexNum = {
			r: 1,
			i: 2,
			mod: function() {
				return Math.sqrt(this.r * this.r + this.i * this.i);
			}
		};
		bindAll(complexNum);
		mod = complexNum.mod;
	});

	it('Bound mod returns number', function() {
		assert.isNumber(mod());
	});
	it('Bound mod returns same as unbound', function() {
		assert.strictEqual(mod(), complexNum.mod());
	});
});

describe('deepEqual', function() {
	/*
	const deepEqual = function(a, b) {
		if (a === b) return true; 
		
		if (a === undefined) return b === undefined;
		if (b === undefined) return a === undefined;
		if (a === null) return b === null;
		if (b === null) return a === null;

		const ta = typeof(a);
		if (ta !== typeof(b)) return false;
		if(ta === 'function') return a.toString() === b.toString();
		if(ta === 'object') {
			const keys_a = Object.keys(a);
			const keys_b = Object.keys(b);
			if (keys_a.length !== keys_b.length) return false;
			for(let i = 0; i < keys_a.length; i++) {
				const key = keys_a[i];
				if (!keys_b.includes(key)) return false;
				if (!deepEqual(a[key], b[key])) return false;
			}
			return true;
		}
		return false;
	};*/

	it('undefined or null', function() {
		assert.isTrue(deepEqual(undefined, undefined));
		assert.isFalse(deepEqual(undefined, 1));
		assert.isFalse(deepEqual(1, null));
		assert.isFalse(deepEqual(undefined, null));
		assert.isTrue(deepEqual(null, null));
	});
	it('number', function() {
		assert.isTrue(deepEqual(1, 1));
		assert.isFalse(deepEqual('1', 1));
		assert.isFalse(deepEqual(1, 2));
	});
	it('string', function() {
		assert.isTrue(deepEqual('1', '1'));
		assert.isFalse(deepEqual(1, '1'));
	});
	it('function', function() {
		assert.isTrue(deepEqual(function(){ return 1; }, function(){ return 1; }));
		assert.isFalse(deepEqual(function(){}, function(){ return 1; }));
		assert.isFalse(deepEqual(function(){}, undefined));
	});
	it('same reference', function() {
		const a = {};
		assert.isTrue(deepEqual(a, a));
	})
	it('empty object', function() {
		const a = {},  b = {};
		assert.isTrue(deepEqual(a, b));
	});
	it('object and null', function() {
		const a = {}, b = null;
		assert.isFalse(deepEqual(a, b));
		assert.isFalse(deepEqual(b, a));
	})
	it('plain object', function() {
		const a = { 
			a: 1 
		};
		const b = { 
			a: 1
		};
		const c = {};
		assert.isTrue(deepEqual(a, b));
		assert.isFalse(deepEqual(a, c));
		assert.isFalse(deepEqual(c, a));
	});
	it('deep object', function() {
		const a = {
			a: function() {},
			b: 5,
			c: {
				d: 'test',
				e: null
			}
		}
		const b = {
			a: function() {},
			b: 5,
			c: {
				d: 'test',
				e: null
			}
		}
		const c = {
			a: function() {},
			b: 5,
			c: {
				d: 'test',
				e: undefined
			}
		}
		assert.isTrue(deepEqual(a, b));
		assert.isTrue(deepEqual(b, a));
		assert.isFalse(deepEqual(a, c));
		assert.isFalse(deepEqual(c, a));
	});
});

describe('toList', function() {
	it('check list', function() {
		const arr = [0, 1, 2];
		assert.containSubset(arr.toList(), {
			head: 0,
			tail: {
				head: 1,
				tail: {
					head: 2
				}
			}
		});
	});
	it('check to array', function() {
		const arr = [0, 1, 2];
		assert.deepEqual(arr.toList().toArray(), arr);
	});
	
	it('check to array #2', function() {
		const arr = [1, 2, 3, 4];
		assert.deepEqual(arr.toList().toArray(), arr);
	});
	it('check nth element', function() {
		const arr = [0, 1, 2];
		const list = arr.toList();
		assert(list.nth(0) === 0);
		assert(list.nth(1) === 1);
		assert(list.nth(2) === 2);
	});
});

describe('extract', function() {
	let obj = null;
	beforeEach(function() {
		obj = {
			a: 1,
			b: '2',
			c: {
				d: [1, 2, 3],
				e: null,
				f: 3
			}
		};
	});
	it('value level 1', function() {
		assert.strictEqual(extract(obj, 'a'), 1);
		assert.strictEqual(extract(obj, 'b'), '2');
	});
	it('object level 1', function() {
		assert.deepEqual(extract(obj, 'c'), obj.c);
	});
	it('value level 2', function(){
		assert.strictEqual(extract(obj, 'c.f'), 3);
		assert.strictEqual(extract(obj, 'c.e'), null);
	});
	it('extract array level 2', function() {
		assert.deepEqual(extract(obj, 'c.d'), obj.c.d);
	});
	it('extract value from the array level 2', function() {
		assert.strictEqual(extract(obj, 'c.d[1]'), obj.c.d[1]);
	});
	it('extract value by key level 1', function() {
		assert.strictEqual(extract(obj, '[\'a\']'), obj.a);
	});
	it('extract value by key level 2', function() {
		assert.strictEqual(extract(obj, '[\'c\'][\'f\']'), obj.c.f);
		assert.strictEqual(extract(obj, '[\'c\'][\'d\'][2]'), obj.c.d[2]);
	});
});

describe('seq', function() {
	/*
	const seq = function(f) {
		const args = [];
		return function() {
			const r = f.apply(null, args);
			args.unshift(r);
			return r;
		}
	};
	*/

	it('simple sequence test', function() {
		var f = seq(function() {
			return arguments.length;
		});
		assert(f() === 0);
		assert(f() === 1);
		assert(f() === 2);
		assert(f() === 3);
	});

	it('arguments order test', function() {
		var args = null;
		var f = seq(function() {
			args = [].slice.call(arguments);
			return args.length;
		});
		f(); assert.deepEqual(args, []);
		f(); assert.deepEqual(args, [0]);
		f(); assert.deepEqual(args, [1, 0]);
		f(); assert.deepEqual(args, [2, 1, 0]);
	});

	it('fibonachi test', function() {
		var fib = seq(function(a, b) { return a + b || 1; });
		assert(fib() === 1);
		assert(fib() === 1);
		assert(fib() === 2);
		assert(fib() === 3);
		assert(fib() === 5);
		assert(fib() === 8);
		assert(fib() === 13);
	});
});

describe('carry', function() {
	it('test 1 + 2 + 3', function() {
		const f = carry(function(a, b, c) { return a + b + c; });
		f(1);
		f(2);
		assert(f(3) === 6);
	});
	it('test 1 + 2 + 3', function() {
		const f = carry(function(a, b, c) { return a + b + c; });
		assert(f(1)(2)(3) === 6);
	})
	it('test 3*4 - 2 + 1', function() {
		const f = carry(function(a, b, c, d) { return a * b - c + d; });
		assert(f(3)(4)(2)(1) === 11);
	})
});