/*
Написать функцию, после выполнения которой, возможны след. операции:
[1, 2, 3].toList() которая превращает массив любых значений в структуру след вида
var list = {
	head: 1,
	tail: {
		head: 2,
		tail: {
			head: 3,
			tail: undefined
		}
	}
}
list.toArray() которая делает обратную операцию => [1, 2, 3]
list.nth(n_elem)  возвращает значение n-го элемента от начала списка.
*/


// toList defined as array prototype

Array.prototype.toList = function(){
    if (this.length > 0) {                                  // if we have something in the array
        var obj = {                                         
            head: this[0],                                  // then add to the object properties head
            tail: this.slice(1 , this.length).toList()     // property tail which is an object as a
        }                                                   // result of recursively called toList for one fist element reduced array
        return obj;
    }                                       
}

// toArray defined as object prototype

Object.prototype.toArray = function (array){                // having an object from this   
    if (!array) { var arr = [] }                            // if this is the first one call then we need to create a result array that we will return
    else {arr = array}                                      // else we already have a result array and just need to add elements to it
    arr.push (this.head);                                   // push head property value to the array
    if(this.tail) {                                         // if tail property exists call itself providing result array and tail to processs
         this.tail.toArray(arr);
    }
    return arr;
}

// nth defined as object prototype

Object.prototype.nth = function (num) {                     
    //console.log ('num = ' + num);                         
    if (num === 0) { return this.head }                          // if num is 0 then we just need to return value of tail property of the current object
    else {                                                  // else we need to call function again for the tail property and decrease num for 1 
        if (this.tail){
            return this.tail.nth(num-1); 
        } else { 
            var err = new Error();                          // if num is greater than elements in list we need to stop and reurn an error
            return err; }
    }
}


Object.prototype.toStr = function (){
    var arr = '';
    arr = arr + (this.head);
    //console.log ('arr = ' + arr);
    if(this.tail) { 
        arr += (this.tail.toStr());
    }
    return arr;
}

const arr = [0, 1, 2];
//console.log (arr.toList());
//console.log (arr);
//console.log (arr.toList().toArray())

var a = [1,2,3,4,5,6,7,8].toList();                     // a = { head: 1, tail: { head: 2, tail: { head: 3, tail: [Object] } } }
//console.log (a);                                        
//console.log(a.toArray());                               // [ 1, 2, 3, 4, 5, 6, 7, 8 ]
console.log (arr.toList().nth(2));                                 // { head: 3, tail: { head: 4, tail: { head: 5, tail: [Object] } } }

const list = arr.toList();
console.log (list.nth(0));
console.log (list.nth(1));
console.log (list.nth(2));
