/*
Напишите функцию deepEqual(val1, val2) которая производит “глубокое” сравнение двух значений, 
val1 и val2 могут быть любого типа (булевы значения, числа, строки, массивы, объекты, функции) 
эквивалентность функций означает что код этих функций - одинаковый.
*/

// we need to compare number, string, boolean, null, undefined, object

function deepEqual(objOne, objTwo){
  
  if (objOne === undefined) {                                                       // if we have undefined value for obj1 
      if (objTwo === undefined) { return true };                                    // and undefined for obj2 then true 
      return false;                                                                 // else false
  }

  if (typeof (objOne) == typeof (objTwo))  {                                        // if we have two objects of the same type
    switch(typeof (objOne)) {                                                       // switch to the corresponding case

    case 'object':                                                                  // if we compare objects of object type we need to compare all the properties 
       
        //console.log ('Objects!');
        //console.log ('objOne:' + objOne.toString());
        //console.log ('objTwo:' + objTwo.toString()); 

        if (objOne == null || objTwo == null) {                                     // if objects are both nulls they are equals
            //console.log ('some of objects is null')
            return (objOne === objTwo) }  

        if (Object.keys(objOne).length != Object.keys(objTwo).length) {             // if objects have different amount of properties they are not equeal
            //console.log ('objects have different amount of properties');
            return false }

        for(var p in objOne){                                                       // compare each property of objOne to corresponding property of objTwo 
          if(objOne.hasOwnProperty(p)){                                             // if there is no p property in objTwo then it is undefined 
              if (!(deepEqual (objOne[p], objTwo[p]))){ return false }              // if property objOne[p] is not equeal objTwo[p] return false
          }
        }
        return true;                                                                // we compared objects and they are equal
    
    case 'string':
        //console.log ('Strings');
        return (objOne === objTwo);

    case 'boolean':
        //console.log ('boolean');
        if (objOne === objTwo){ return true }
        else { return false }
        break;

    case 'number':
        //console.log ('numbers');
        if (objOne === objTwo){ return true }
        else { return false }
        break;

    case 'function':                                                                // compare code as aparts of function string definition like { ... code ...}
        //console.log ('functions');
        return (deepEqual(objOne.toString().match(/\{([\s\S])*\}/)[0], objTwo.toString().match(/\{([\s\S])*\}/)[0]));
    

    default:
        //console.log ('break');
    }                                                                               // end of switch block
  } else { return false }                                                           // if objects are different types they are not equal
}

// objects to compare

var objectA = {
  e:1,
  k:1,
};

var objectF = {
  e:1,
  f:1,
};

var objectA1 = {a:1, b: objectA};

var objectB = {
  a: objectA1,
  b: true
};

var objectC = {
  a: objectA1,
  b: true
};

var objectD = {
    a: {e:1,k:3},
    h: objectB,
    b: true
};

var objectE = {
    a: {e:1,k:3},
    b: true,
    h: objectB
};


function mod (i,r){console.log(r);}
function modTwo (i,r){console.log(r);}


// testing (assert is possible here)
/*
console.log (deepEqual([1,3,4], [1,3,4])); // true
console.log (deepEqual (33,33));           // true
console.log (deepEqual (null,null));       // true
console.log (deepEqual ('Vv', 'vv'));      // false
console.log (deepEqual (true, true));      // true
console.log (deepEqual(mod, modTwo));      // true
console.log (deepEqual (objectB,objectC)); // true
console.log (deepEqual (objectA,objectF)); // false
console.log (deepEqual (objectD,objectE)); // true
*/
console.log (deepEqual(undefined, undefined));
console.log (deepEqual(undefined, 1));
console.log (deepEqual(1, null));
console.log (deepEqual(undefined, null));
console.log (deepEqual(null, null));

