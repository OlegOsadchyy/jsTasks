/*
Написать функцию logIt(obj) на вход которой поступает объект вида (пример)
var complexNum = { r: 1, i: 2, mod: function() { return Math.sqrt(this.r * this.r + this.i * this.i); }, add: function(r, i) { this.r += r; this.i += i; } };
Функция модифицирует его таким образом, что потом, при вызове каждого метода, происходит распечатка в консоли браузера имени вызванного метода и его аргументов
*/


var complexNum = { 
    r: 1, 
    i: 2, 
    mod: function() { 
        //console.log (' this is auth mod');
        return Math.sqrt(this.r * this.r + this.i * this.i); 
    }, 
    add: function(r, i) { this.r += r; this.i += i; } 
};

function logIt (obj) {                                                 // check the object
    for (var name in obj) {                                            // every property
        //console.log(name + ' has type ' + typeof(obj[name]));
        if (typeof(obj[name]) === 'function') {                         // and modify each method by modifyMethod function that wraps existed method
            obj[name] = modifyMethod(obj, name); 
        }
    };
}

function modifyMethod(obj, name) {                                      // having object and name of method
    var authFunc = obj[name];                                           // save method function to a variable 
    var funcCover =  function () {                                      // then wrap function with a code
        console.log (' Method ' + name + ' has been called with the following arguments:'); // that prints out the name of Method // and arguments
        for (var index = 0; index < arguments.length; index++){
           console.log(arguments[index]);
        }
        return authFunc.apply(this, arguments);                         // then code calls method using apply and arguments
    }
    return funcCover;                                                   // return wrapper
}

logIt (complexNum);
complexNum.mod(10,11,12);                                               // Method mod has been called with the following arguments: 10 11 12 
complexNum.add (100, 200);                                              // Method add has been called with the following arguments: 100 200
console.log (complexNum.r.toString() + ' ' + complexNum.i);             // 101 202
console.log (complexNum.mod());                                         // Method modhas been called with the following arguments: 225.84286572747877
